from flask import Flask
from flask_restful import Resource, Api
import pg8000

app=Flask(__name__)
api=Api(app)
connection=pg8000.connect(user="adminhx6xpwt",host='127.7.199.2',database='krnuon',password='WqQRPTD327XK')
   
gamesdict={}

class Players(Resource):
    def get(self,nick):
        nicknames=[]
        try:
            cursor=connection.cursor()
            cursor.execute("SELECT WhoIsOnline();")
            connection.commit()
        except:
            return {'state':'failed','nicknames':[]}
        while True:
            buf=cursor.fetchone()
            if buf==None:
                break
            if not buf==[nick]:
                nicknames.extend(buf)
        return {'state':'OK','online':nicknames}
        
        
    def put(self,nick):
        try:
            cursor=connection.cursor()
            cursor.execute("SELECT IAmOnline('"+ nick +"');")
            connection.commit()
        except:
            cursor.close()
            return {'state':'failed'}
        cursor.close()
        return {'state':'OK'}
     
    def delete(self,nick):
        try:
            cursor=connection.cursor()
            cursor.execute("SELECT IAmNotOnline('"+ nick +"');")
            connection.commit()
        except:
            cursor.close()
            return {'state':'failed'}
        cursor.close()
        return {'state':'OK'}
             
class Invites(Resource):
    def get(self,nickfrom,nickto):
        inviter=[]
        cursor=connection.cursor()
        cursor.execute("SELECT AmIInvited('" + nickfrom + "');")
        inviter.extend(cursor.fetchone())
        if inviter==None:
            cursor.close()
            return {'inviter':'None'}
        else:
            cursor.close()
            print({'inviter':inviter[0]})
            return {'inviter':inviter[0]}
            
    def post(self,nickfrom,nickto):
        cursor=connection.cursor()
        cursor.execute("SELECT Invite('"+nickfrom+"','"+nickto+"');")
        if cursor.fetchone()==[False]:
            cursor.close()
            print({'Invited':'False'})
            return {'Invited':'False'}
        else:
            cursor.close()
            print({'Invited':'True'})
            return {'Invited':'True'}
            
class Games(Resource):
    def post(self,nick1,nick2):
        cursor=connection.cursor()
        print('Cursor created')
        cursor.execute("SELECT GameBegins('" + nick1 + "','" + nick2 + "');")
        print('GameBegins executed')
        buf_id = cursor.fetchone()
        print('buf_id zugewiesen')
        cursor2=connection.cursor()
        print(buf_id[0])
        cursor.execute(
            "DELETE FROM DictErsatz WHERE Nickname='"+str(nick1)+"';" )
        cursor2.execute(
            "INSERT INTO DictErsatz(Nickname,Id) VALUES ('"+nick2+"',"+str(buf_id[0])+");")
        print('DictErsatz inserted')
        cursor2.close()
        cursor.close()
        print('Result:', {'Id':buf_id[0]} )
        return {'Id':buf_id[0]}
        
    def get(self,nick1,nick2):
        """Hier tauschen sich die nick1 und nick2, die bei post gespeichert
           sind, und der zweite Spieler bekommt ID des Spieles aus dictionary"""
        cursor=connection.cursor()
        cursor.execute(
            "SELECT Id FROM DictErsatz WHERE Nickname='"+str(nick1)+"';")
        game_id=cursor.fetchone()
        cursor.close()
        if game_id==None:
            print({'game_id':'None'})
            return {'game_id':'None'}
        print({'game_id':game_id[0]})
        return {'game_id':game_id[0]}
    
class Gaming(Resource):
    def post(self,game_id,mask):
        try:
            cursor=connection.cursor()
            cursor.execute("SELECT NextMove(" + str(game_id) + ",'" + mask + "');")
        except:
            cursor.close()
            print({'state':'failed'})
            return {'state':'failed'}
        cursor.close()
        print({'state':'OK'})
        return {'state':'OK'}
        
    def get(self,game_id,mask):
        try:
            cursor=connection.cursor()
            print ('Cursor created')
            cursor.execute("SELECT ReadMove(" + str(game_id) + ");")
            print('Readmove gemacht')
            buf=cursor.fetchone()
            print('buf zugewiesen')
            cursor.close()
            print({'state':'OK','mask':buf[0]})
            return {'state':'OK','mask':buf[0]}
        except:
            cursor.close()
            print({'state':'failed'})
            return {'state':'failed'}
                   
api.add_resource(Players,'/players/<string:nick>')
api.add_resource(Invites,'/invites/<string:nickfrom>/to/<string:nickto>')
api.add_resource(Games,'/games/<string:nick1>/<string:nick2>')
api.add_resource(Gaming,'/<int:game_id>/<string:mask>')

if __name__=='__main__':
    app.run()
        
        
### Was ist das? ###

* Das ist mein Projekt, ein kleines Online-Spiel (Tic-Tac-Toe). Da ist ein Delphi Client, REST, das mit Python und Framework Flask geschrieben wurde und Datenbank mit vielen PL/pgSQL-Prozeduren. Das Projekt wurde auf dem openshift.com gehostet. Das Project hat Bugs und prüft das Ende des Spieles nicht.

### What is this repository for? ###

* It's my project, a small online Tic-Tac-Toe game. There are Delphi client, Python REST and PL/pgSQL database server. REST is written with a framework Flask. It was deployed of openshift.com. There are several bugs and no end of game. 
unit registration;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TfRegistration }

  TfRegistration = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    flag:boolean;
  public
    { public declarations }
  end;

var
  fRegistration: TfRegistration;

implementation

{$R *.lfm}

{ TfRegistration }

procedure TfRegistration.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  case Key of
       'a'..'z':;
       'A'..'Z':;
       '0'..'9':;
       {<-}   #8 :;
       {ENTER}#13:begin
                   if Edit1.Text<>'' then flag:=true;
                   Self.Close;
                  end;
  else begin
       Edit1.ShowHint:=true;
       Edit1.Hint:='You can input letters and numbers only';
       Key:=#0;
       end;
  if Length(Edit1.Text)>49 then
     begin
       Key:=#0;
     end;
  end;
end;

procedure TfRegistration.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
     if not flag then CloseAction:=caNone;
end;

procedure TfRegistration.FormCreate(Sender: TObject);
begin
  flag:=false;
end;

end.


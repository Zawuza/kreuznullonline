unit waitforainvite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, ExtCtrls;

type

  { TfWait }

  TfWait = class(TForm)
     ProgressBar1:TProgressBar;
     Timer1: TTimer;
     Label1: TLabel;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  fWait: TfWait;
  closable:boolean;

implementation

{$R *.lfm}

{ TfWait }

procedure TfWait.FormShow(Sender: TObject);
begin
  closable:=false;
  ProgressBar1.Position:=0;
  Timer1.Enabled:=true;
end;

procedure TfWait.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if not closable then CloseAction:=caNone;
end;

procedure TfWait.Timer1Timer(Sender: TObject);
begin
  if ProgressBar1.Position=15 then
     begin
       closable:=true;
       Close;
     end;
  ProgressBar1.Position:=ProgressBar1.Position+1;
end;

end.


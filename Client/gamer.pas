unit gamer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type TGamer = class
  Nickname: string;
  Inviter:string;
  CurrentGameID:integer;
  Opponent: string;
  Kreuze: boolean;
  constructor Create;
end;

implementation

constructor TGamer.Create;
begin
  inherited Create;
  Inviter:='';
  Kreuze:=false;
end;

end.


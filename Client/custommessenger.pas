unit CustomMessenger;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TfMsg }

  TfMsg = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure ShowMessageCustom(MessageCustom: string);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  fMsg: TfMsg;

implementation

procedure TfMsg.ShowMessageCustom(MessageCustom: string);
begin
  Label1.Caption:=MessageCustom;
  Self.ShowModal;
end;

procedure TfMsg.Button1Click(Sender: TObject);
begin
  Self.Close;
end;

{$R *.lfm}

end.


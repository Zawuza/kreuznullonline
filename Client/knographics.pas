unit KNOGraphics;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

type TKNOFieldArray = array [1..9] of integer;

type TKNOField = class
  private
  Canvas:TCanvas;
  Left,Right,Top,Bottom: integer;
  CellWidth,CellHeight : integer;
  function ParseMask: TKNOFIeldArray;
  public
  CurrentState:string;
  Mask:string;
  MyMove:boolean;
  Active:boolean;
  constructor Create(IntoCanvas:TCanvas);
  procedure Draw;
  function KeyPress(x,y:integer):integer;
  destructor Destroy; override;
end;

implementation

constructor TKNOField.Create(IntoCanvas:TCanvas);
var Font:TFont;
begin
  Font:=TFont.Create;
  Font.Color:=clBlack;
  Font.Size:=40;
  inherited Create;
  Canvas:=IntoCanvas;
  CurrentState:='Game begins';
  Canvas.Font:=Font;
  Mask:='000000000';
end;

procedure TKNOFIeld.Draw;
var Cells:TKNOFIeldArray;
    i: integer;
    x,y:integer;
begin
  {Feldgroessen bestimmen}
  Left:=Trunc(Canvas.Width/6);
  Right:=Canvas.Width-Left;
  Top:=Trunc(Canvas.Height/8);
  Bottom:=Canvas.Height-Top;
  CellWidth:=Trunc((Right-Left)/3);
  CellHeight:=Trunc((Bottom-Top)/3);
  {Feld zeichnen}
  Canvas.MoveTo(Left+CellWidth,Top);
  Canvas.LineTo(Left+CellWidth,Bottom);
  Canvas.MoveTo(Right-CellWidth,Top);
  Canvas.LineTo(Right-CellWidth,Bottom);
  Canvas.MoveTo(Left,Top+CellHeight);
  Canvas.LineTo(Right,Top+CellHeight);
  Canvas.MoveTo(Left,Bottom-CellHeight);
  Canvas.LineTo(Right,Bottom-CellHeight);
  {Mask parsen und dann Kreuze und Nulle zechnen}
  Cells:=ParseMask;
  for i:=1 to 9 do
      begin
      case i of
           1:begin
               x:=Left+Trunc(CellWidth/2)-15;
               y:=Top+Trunc(CellHeight/2)-15;
           end;
           2:begin
               x:=Left+Trunc(3*CellWidth/2)-15;
               y:=Top+Trunc(CellHeight/2)-15;
           end;
           3:begin
               x:=Right-Trunc(CellWidth/2)-15;
               y:=Top+Trunc(CellHeight/2)-15;
           end;
           4:begin
               x:=Left+Trunc(CellWidth/2)-15;
               y:=Top+Trunc(3*CellHeight/2)-15;
           end;
           5:begin
               x:=Left+Trunc(3*CellWidth/2)-15;
               y:=Top+Trunc(3*CellHeight/2)-15;
           end;
           6:begin
               x:=Right-Trunc(CellWidth/2)-15;
               y:=Top+Trunc(3*CellHeight/2)-15;
           end;
           7:begin
               x:=Left+Trunc(CellWidth/2)-15;
               y:=Bottom-Trunc(CellHeight/2)-15;
           end;
           8:begin
               x:=Left+Trunc(3*CellWidth/2)-15;
               y:=Bottom-Trunc(CellHeight/2)-15;
           end;
           9:begin
               x:=Right-Trunc(CellWidth/2)-15;
               y:=Bottom-Trunc(CellHeight/2)-15;
           end;
      end;
      if (x>0) and (y>0) then
      begin
         case Cells[i] of
              0:;
  {Kreuz}     1:Canvas.TextOut(x,y,'X');
  {Null}      2:Canvas.TextOut(x,y,'0');
         end;
      end;
      end;
end;

destructor TKNOField.Destroy;
begin
  Canvas:=nil;
  inherited Destroy;
end;

function TKNOField.ParseMask: TKNOFieldArray;
begin
  if Mask='' then exit;
  Result[1]:=StrToInt(Mask[1]);
  Result[2]:=StrToInt(Mask[2]);
  Result[3]:=StrToInt(Mask[3]);
  Result[4]:=StrToInt(Mask[4]);
  Result[5]:=StrToInt(Mask[5]);
  Result[6]:=StrToInt(Mask[6]);
  Result[7]:=StrToInt(Mask[7]);
  Result[8]:=StrToInt(Mask[8]);
  Result[9]:=StrToInt(Mask[9]);
end;

function TKNOField.KeyPress(x,y:integer):integer;
var Index: integer;
begin
  Result:=0;
  if (x>Left) and (x<Left+CellWidth) then               Index:=1;
  if (x>Left+CellWidth) and (x<Right-CellWidth) then    Index:=2;
  if (x>Right-CellWidth) and (x<Right) then             Index:=3 ;
  if (y>Top) and (y<Top+CellHeight) then                Result:=Index;
  if (y>Top+CellHeight) and (y<Bottom-CellHeight) then  Result:=Index+3;
  if (y>Bottom-CellHeight) and (y<Bottom) then          Result:=Index+6;
end;

end.


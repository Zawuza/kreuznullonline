unit threadsingame;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DataLoader,gamer, Dialogs, crt, KNOGraphics;

type TDemon = class(TThread)
  protected
  procedure Execute; override;
  public
  DL:TDataLoader;
  GamerData:TGamer;
  constructor Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
  procedure AddExt(DatLoad:TDataLoader; G:TGamer);
  end;

TGameDemon = class(TDemon)
  protected
  procedure Execute; override;
  public
  KNOF:TKNOField;
  MyTurn:boolean;
  NewMask:string;
  constructor Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
  procedure SetTurn(AmIInviter:boolean);
  procedure AddField(Field:TKNOField);
end;

implementation

{TDemon}

procedure TDemon.AddExt(DatLoad:TDataLoader; G:TGamer);
begin
  DL:=DatLoad;
  GamerData:=G;
end;

constructor TDemon.Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
begin
  inherited Create(CreateSuspended);
  FreeOnTerminate:=true;
end;

procedure TDemon.Execute;
begin
  while true do
     begin
       Delay(5000);
       if not DL.SendOnline(GamerData.Nickname) then ShowMessage('Connection closed. Restart the game');
       GamerData.Inviter:=DL.CheckInvite(GamerData.Nickname);
     end;
end;

{TGameDemon}

constructor TGameDemon.Create(CreateSuspended:boolean;const StackSize: SizeUInt = DefaultStackSize);
begin
  inherited Create(CreateSuspended);
  NewMask:='000000000';
end;

procedure TGameDemon.Execute;
begin
  while true do
     begin
       Delay(5000);
       if not MyTurn then
          begin
          KNOF.CurrentState:=GamerData.Opponent + ' turns';
          NewMask:=DL.ReadMove(GamerData.CurrentGameID);
          if NewMask=KNOF.Mask then continue;
          KNOF.Mask:=NewMask;
          KNOF.CurrentState:='You turn!';
          MyTurn:=true;
          end
       else
          begin
           KNOF.CurrentState:='You turn';
          end;
     end;
end;

procedure TGameDemon.SetTurn(AmIInviter: boolean);
begin
  MyTurn:= not AmIInviter;
end;

procedure TGameDemon.AddField(Field:TKNOField);
begin
  KNOF:=Field;
end;

end.


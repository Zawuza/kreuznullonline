unit DataLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FPHTTPCLient, fpjson, jsonparser, Dialogs;

const Server = 'http://krnuon-zawuza.rhcloud.com';

type TDataLoader = class
     private
     jData: TJSONData;
     jObjt: TJSONObject;
     public
     constructor Create;
     function LoadOnlineUsers(Nickname:string): TStringList;
     function SendOnline(Nickname:string):boolean;
     function SendOffline(Nickname:string):boolean;
     procedure Invite(const Nickfrom,NickTo:string);
     function CheckGame(NickMy,NickFrom: string; var GameID:integer): boolean;
     function CheckInvite(Nickname:string): string;
     function CreateGame(NicknameMe,NicknameTO:string): integer;
     function FindGame (NicknameMe,NicknameTO:string): integer;
     function ReadMove(ID: integer): string;
     procedure SendMove(ID:integer; Mask: string);
     destructor Destroy; override;
end;

implementation

constructor TDataLoader.Create;
begin
  inherited Create;
  jObjt:=TJSONObject.Create;
end;

function TDataLoader.LoadOnlineUsers(Nickname:string): TStringList;
var List: TStringList;
    Response,Value:string;
    JArray: TJSONArray;
    i:integer;
    Client: TFPHTTPCLIENT;
begin
  Client:=TFPHTTPClient.Create(nil);
  try
  List:=TStringList.Create;
  jArray:=TJSONArray.Create;
  Response:=Client.Get(Server + '/players/' + Nickname);
  jData:=GetJSON(Response);
  jObjt:=TJSONObject(jData);
  Value:=jObjt.Get('state');
  if Value='failed' then
     begin
       List.Add('Failed');
       Result:=List;
       exit;
     end;
  jArray:=jObjt.Arrays['online'];
  for i:=0 to jArray.Count-1 do
      begin
        List.Add(jArray.Items[i].AsString);
      end;
  Result:=List;
  Client.Free;
  except
  List.Add('Failed');
  Result:=List;
  Client.Free;
  end;
end;

function TDataLoader.SendOnline(Nickname:string):boolean;
var Response, Value: string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
    Response:=Client.Put(Server + '/players/' + Nickname);
    jData:=GetJSON(Response);
    jObjt:=TJSONObject(jData);
    Value:=jObjt.Get('state');
    if Value='OK' then Result:=true
      else Result:=false;
    Client.Free;
  except
    Result:=false;
    Client.Free;
  end;
end;

function TDataLoader.SendOffline(Nickname:string):boolean;
var Response,Value:string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
    Response:=Client.Delete(Server + '/players/' + Nickname);
    jData:=GetJSON(Response);
    jObjt:=TJSONObject(jData);
    Value:=jObjt.Get('state');
    if Value='OK' then Result:=true
    else Result:=false;
    Client.Free;
  except
    Result:=false;
    Client.Free;
  end;
end;

procedure TDataLoader.Invite(const Nickfrom,NickTo:string);
var Response: string;
    Value: string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
    Response:=Client.Post(Server+'/invites/'+Nickfrom+'/to/' + NickTo);
    jData:=GetJSON(Response);
    jObjt:=TJSONObject(jData);
    Value:=jObjt.Get('Invited');
    if Value='False' then raise Exception.Create('');
    Client.Free;
  except
    ShowMessage('Failure by inviting. Check connection and restart the game');
    Client.Free;
  end;
end;

function TDataLoader.CheckGame(NickMy,NickFrom: string; var GameID:integer): boolean;
var Response: string;
    Value: string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
    Response:=Client.Get(Server + '/games/' + Nickmy + '/' + NickFrom);
    jData:=GetJSON(Response);
    jObjt:=TJSONObject(jData);
    Value:=jObjt.Get('game_id');
    if Value='None' then
       Result:=false
    else
        begin
          Result:=true;
          GameID:=StrToInt(Value);
        end;
    Client.Free;
  except
    ShowMessage('Connection failure');
    Result:=false;
    Client.Free;
  end;
end;

function TDataLoader.CheckInvite(Nickname:string): string;
var Response: string;
    Value: string;
    jStr:TJSONstringType;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
  Response:=Client.Get(Server + '/invites/' + Nickname + '/to/NONE');
  jData:=GetJSON(Response);
  jObjt:=TJSONObject(jData);
  jstr:='';
  Value:=jObjt.Get('inviter',jStr);
  Result:=Value;
  Client.Free;
  except
  Client.Free;
  end;
end;

function TDataLoader.CreateGame(NicknameMe,NicknameTO:string): integer;
var   Response: string;
      Value: integer;
      Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
  Response:=Client.Post(Server + '/games/' + NicknameMe + '/' + NicknameTO);
  jData:=GetJSON(Response);
  jObjt:=TJSONObject(jData);
  Value:=jObjt.Get('Id',0);
  if Value=0 then raise Exception.Create('');
  Result:=Value;
  Client.Free;
  except
  ShowMessage('Connection failed. Is it bug???');
  Client.Free;
  end;
end;

function TDataLoader.FindGame (NicknameMe,NicknameTO:string): integer;
var   Response: string;
      Value: integer;
      Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
  try
  Response:=Client.Get(Server + '/games/' + NicknameMe + '/' + NicknameTo);
  jData:=GetJSON(Response);
  jObjt:=TJSONObject(jData);
  Value:=jObjt.Get('game_id',0);
  if Value=0 then raise Exception.Create('');
  Result:=Value;
  Client.Free;
  except
  ShowMessage('Connection failed, nope :(');
  Client.Free;
  end;
end;

function TDataLoader.ReadMove(ID:integer): string;
var Response: string;
    Value: string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
   try
   Response:=Client.Get(Server + '/' + IntToStr(ID) + '/NONE');
   jData:=GetJSON(Response);
   jObjt:=TJSONObject(jData);
   Value:=jObjt.Get('state');
   if Value='OK' then
      begin
      Value:=jObjt.Get('mask',TJSONStringType(''));
      Result:=Value;
      end
   else Self.ReadMove(ID);
   Client.Free;
   except
     ShowMessage('Gaming-failure. Restart the game');
     Client.Free;
   end;
end;

procedure TDataLoader.SendMove(ID:integer; Mask: string);
var Response: string;
    Value: string;
    Client: TFPHTTPCLIENT;
begin
   Client:=TFPHTTPClient.Create(nil);
   try
   Response:=Client.Post(Server + '/' + IntToStr(ID) + '/' + Mask);
   jData:=GetJSON(Response);
   jObjt:=TJSONObject(jData);
   Value:=jObjt.Get('state');
   if Value='failed' then raise Exception.Create('');
   Client.Free;
   except
     ShowMessage('Gaming-failure. Restart the game');
     Client.Free;
   end;
end;

destructor TDataLoader.Destroy;
begin
   jObjt.Free;
   inherited Destroy;
end;

end.


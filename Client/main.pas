unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, jsonparser, DataLoader, gamer,registration, threadsingame, waitforainvite,
  custommessenger, KNOGraphics;

type

  { TfMain }

  TfMain = class(TForm)
    AcceptButton: TButton;
    RollbackButton: TButton;
    InviteLabel: TLabel;
    GameStateLabel: TLabel;
    ListBoxPlayers: TListBox;
    PlayButton: TButton;
    KNONameLabel: TLabel;
    PB: TPaintBox;
    PaintTimer: TTimer;
    CheckTimer: TTimer;
    procedure AcceptButtonClick(Sender: TObject);
    procedure CheckTimerTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBoxPlayersDblClick(Sender: TObject);
    procedure PBMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PBPaint(Sender: TObject);
    procedure PlayButtonClick(Sender: TObject);
    procedure PrepareToGame;
    procedure PaintTimerTimer(Sender: TObject);
    procedure RollbackButtonClick(Sender: TObject);
    procedure UpdateList;
    procedure Rollback;
  private
    { private declarations }
  public
    DataLoader: TDataLoader;
    Gamer:TGamer;
    KNOField:TKNOField;
    FlagNotPaint: boolean;
    AmIInviter: boolean;
    Demon:TDemon;
    GameDemon:TGameDemon;
  end;

var
  fMain: TfMain;

implementation

{$R *.lfm}

{ TfMain }

{Menu Interface methods}

procedure TfMain.FormResize(Sender: TObject);
begin
  KNONameLabel.Left:=Trunc(Width/2)-Trunc(KNONameLabel.Width/2);
  PlayButton.Left:=Trunc(Width/2)-Trunc(PlayButton.Width/2);
  PlayButton.Top:=Trunc(Height/2);
  ListBoxPlayers.Left:=Trunc(Width/2)-Trunc(ListBoxPlayers.Width/2);
  ListBoxPlayers.Top:=KnoNameLabel.Top+150;
  InviteLabel.Left:=Trunc(Width/2)-Trunc(InviteLabel.Width/2);
  ListBoxPlayers.Height:=InviteLabel.Top-ListBoxPlayers.Top-100;
  AcceptButton.Left:=Trunc(Width/2)-Trunc(AcceptButton.Width/2);
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
     DataLoader:=TDataLoader.Create;
     Gamer:=TGamer.Create;
     Demon:=TDemon.Create(true);
     GameDemon:=TGameDemon.Create(true);
     FlagNotPaint:=true;
     AmIInviter:=false;
end;

procedure TfMain.CheckTimerTimer(Sender: TObject);
begin
  if Gamer.Inviter<>'' then
     begin
       InviteLabel.Caption:=Gamer.Inviter + ' has invited you';
       AcceptButton.Enabled:=true;
     end
  else
     begin
       InviteLabel.Caption:='Nobody has invited you';
       AcceptButton.Enabled:=false;
     end;
end;

procedure TfMain.AcceptButtonClick(Sender: TObject);
begin
  Gamer.Opponent:=Gamer.Inviter;
  AmIInviter:=false;
  Gamer.CurrentGameID:=DataLoader.CreateGame(Gamer.Nickname,Gamer.Opponent);
  Gamer.Kreuze:=true;
  DataLoader.SendMove(Gamer.CurrentGameID,'000000000');
  PrepareToGame;
end;

procedure TfMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  Demon.Terminate;
  DataLoader.SendOffline(Gamer.Nickname);
  Gamer.Free;
  {GameDemon.Terminate;}
  CloseAction:=caFree;
end;

procedure TfMain.FormShow(Sender: TObject);
var Nickname_local:string;
begin
  FormResize(Self);
  while true do
     begin
     fRegistration.ShowModal;
     Nickname_local:=fRegistration.Edit1.Text;
     if not DataLoader.SendOnline(Nickname_local) then
        fMsg.ShowMessageCustom('Failed. Check your connection')
     else break;
     end;
  Gamer.Nickname:=fRegistration.Edit1.Text;
  fRegistration.Free;
  Demon.AddExt(DataLoader,Gamer);
  Demon.Start;
end;

procedure TfMain.ListBoxPlayersDblClick(Sender: TObject);
var nicknamebuf:string;
    saveid: integer;
begin
  saveid:=0;
  try
  nicknamebuf:=ListBoxPlayers.Items[ListBoxPlayers.ItemIndex];
  except
    exit;
  end;
  if nicknamebuf='Double click to update' then
     begin
       UpdateList;
       exit;
     end;
  DataLoader.Invite(Gamer.Nickname,nicknamebuf);
  fWait.ShowModal;
  if DataLoader.CheckGame(Gamer.Nickname,nicknamebuf,saveid)=true then
     begin
       Gamer.Opponent:=nicknamebuf;
       AmIInviter:=true;
       Gamer.CurrentGameID:=DataLoader.FindGame(Gamer.Nickname,Gamer.Opponent);
       PrepareToGame;
     end
  else
     begin
     fMsg.ShowMessageCustom(nicknamebuf + ' has not accepted your invite :(');
     UpdateList;
     end;
end;

procedure TfMain.PBMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var Index:integer;
    Maskbuf: string;
begin
  if GameDemon.MyTurn then
     begin
          Index:=KNOField.KeyPress(X,Y);
          if Index=0 then exit;
          Maskbuf:=KNOField.Mask;
          case Gamer.Kreuze of
               true:Maskbuf[Index]:='1';
               false:Maskbuf[Index]:='2';
          end;
          KNOField.Mask:=Maskbuf;
          DataLoader.SendMove(Gamer.CurrentGameID,Maskbuf);
          GameDemon.MyTurn:=false;
     end;
end;

procedure TfMain.PlayButtonClick(Sender: TObject);
begin
  UpdateList;
  KNONameLabel.Font.Size:=24;
  KNONameLabel.Caption:='Select a online player';
  PlayButton.Visible:=false;
  ListBoxPlayers.Visible:=true;
  InviteLabel.Visible:=true;
  AcceptButton.Visible:=true;
end;

procedure TfMain.UpdateList;
begin
  ListBoxPlayers.Clear;
  ListBoxPlayers.Items:=DataLoader.LoadOnlineUsers(Gamer.Nickname);
  ListBoxPlayers.Items.Add('Double click to update');
end;

{Game methods}

procedure TfMain.PrepareToGame;
begin
  RollbackButton.Visible:=true;
  Demon.Suspended:=true;
  DataLoader.SendOffline(Gamer.Nickname);
  KNOField:=TKNOField.Create(PB.Canvas);
  GameDemon:=TGameDemon.Create(true);
  GameDemon.AddExt(DataLoader,Gamer);
  GameDemon.SetTurn(AmIInviter);
  GameDemon.AddField(KNOField);
  GameDemon.Start;
  KNOField.Active:=true;
  KNONameLabel.Visible:=false;
  ListBoxPlayers.Visible:=false;
  InviteLabel.Visible:=false;
  AcceptButton.Visible:=false;
  PaintTimer.Enabled:=true;
  FlagNotPaint:=false;
  GameStateLabel.Visible:=true;
end;

procedure TfMain.PaintTimerTimer(Sender: TObject);
begin
  PBPaint(PaintTimer);
end;

procedure TfMain.RollbackButtonClick(Sender: TObject);
begin
  Rollback;
end;

procedure TfMain.PBPaint(Sender: TObject);
begin
  if FlagNotPaint then exit;
  KNOField.Draw;
  GameStateLabel.Caption:=KNOField.CurrentState;
end;

procedure TfMain.Rollback;
begin
  GameStateLabel.Visible:=false;
  Demon.Suspended:=false;
  FlagNotPaint:=true;
  PaintTimer.Enabled:=false;
  AcceptButton.Visible:=true;
  InviteLabel.Visible:=true;
  ListBoxPlayers.Visible:=true;
  UpdateList;
  KNONameLabel.Visible:=true;
  KNOField.Active:=false;
  GameDemon.Terminate;
  KNOField.Destroy;
  DataLoader.SendOnline(Gamer.Nickname);
  RollbackButton.Visible:=false;
  Gamer.Opponent:='';
  Gamer.Kreuze:=false;
  Gamer.CurrentGameID:=0;
  Gamer.Inviter:='';
  PB.Repaint;
end;

end.


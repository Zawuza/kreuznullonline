CREATE ROLE Executor LOGIN;
ALTER ROLE Executor WITH PASSWORD 'fffgggexecute';
GRANT EXECUTE ON FUNCTION thegame.IamOnline(VARCHAR(50)),
thegame.IamNotOnline(VARCHAR(50)),thegame.WhoIsOnline(),
thegame.GameBegins(VARCHAR(50),VARCHAR(50)), thegame.NextMove(INTEGER,CHAR(9)), 
thegame.ReadMove(INTEGER), thegame.Invite(VARCHAR(50),VARCHAR(50)),
thegame.AmIInvited(VARCHAR(50)) TO Executor;

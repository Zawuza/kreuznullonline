BEGIN TRANSACTION;
CREATE TABLE thegame.Players
(
    Nickname VARCHAR(50) PRIMARY KEY NOT NULL,
    LatestNotification TIMESTAMP NOT NULL
);

CREATE TABLE thegame.Invites
(
    Id SERIAL PRIMARY KEY NOT NULL,
    PlayerFrom VARCHAR(50) NOT NULL REFERENCES thegame.Players(Nickname) ON DELETE CASCADE,
    PlayerTo VARCHAR(50) NOT NULL REFERENCES thegame.Players(Nickname) ON DELETE CASCADE,
    InviteTime TIMESTAMP NOT NULL  
);

CREATE TABLE thegame.Games
(
    Id SERIAL PRIMARY KEY NOT NULL,
    Gamer1 VARCHAR(50) NOT NULL,
    Gamer2 VARCHAR(50) NOT NULL,
    Move INTEGER NOT NULL,
    Mask CHAR(9)
);
COMMIT;
﻿CREATE OR REPLACE FUNCTION thegame.IamOnline(VARCHAR(50)) RETURNS void
AS $$
DECLARE
Nick ALIAS FOR $1;
Find VARCHAR(50);
BEGIN
Find:=(SELECT Nickname FROM thegame.Players WHERE Nickname=Nick);
IF COALESCE(Find,'')='' THEN 
  BEGIN
     INSERT INTO thegame.Players(Nickname,LatestNotification) VALUES (Nick,NOW());
  END;
ELSE
  BEGIN
     UPDATE thegame.Players SET LatestNotification=NOW() WHERE Nickname=Nick;
END;
END IF;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.IamNotOnline(VARCHAR(50)) RETURNS void
AS $$
DECLARE
Nick ALIAS FOR $1;
BEGIN
IF EXISTS (SELECT Nickname FROM thegame.Players WHERE Nickname=Nick) THEN
BEGIN
   DELETE FROM thegame.Players WHERE Nickname=Nick;
END; 
END IF;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.WhoIsOnline() RETURNS SETOF VARCHAR(50)
AS $$
DECLARE
--nothing
BEGIN
DELETE FROM thegame.Players WHERE (NOW()-LatestNotification)>INTERVAL'00:01:00';
RETURN QUERY SELECT Nickname FROM thegame.Players;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.GameBegins(VARCHAR(50),VARCHAR(50)) RETURNS INTEGER
AS $$
DECLARE
Player1 ALIAS FOR $1;
Player2 ALIAS FOR $2;
Result INTEGER;
BEGIN
INSERT INTO thegame.Games(Gamer1,Gamer2,Move) VALUES (Player1,Player2,0) Returning Id INTO Result;
RETURN Result;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.NextMove(INTEGER,CHAR(9)) RETURNS void
AS $$
DECLARE
IdOfGame ALIAS FOR $1;
NewMask ALIAS FOR $2;
BEGIN
UPDATE thegame.Games SET Move=Move+1, Mask=NewMask WHERE Id=IdOfGame;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.ReadMove(INTEGER) RETURNS CHAR(9) 
AS $$
DECLARE
IdOfGame ALIAS FOR $1;
BEGIN
RETURN (SELECT Mask FROM thegame.Games WHERE Id=IdOfGame);
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.Invite(VARCHAR(50),VARCHAR(50)) RETURNS BOOLEAN
AS $$
DECLARE
NickFrom ALIAS FOR $1;
NickTo ALIAS FOR $2;
BEGIN
INSERT INTO thegame.Invites(PlayerFrom,PlayerTo,InviteTIme) VALUES (NickFrom,NickTo,NOW());
RETURN true;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;

CREATE OR REPLACE FUNCTION thegame.AmIInvited(VARCHAR(50)) RETURNS VARCHAR(50)
AS $$
DECLARE
Nick ALIAS FOR $1;
Inviter VARCHAR(50);
BEGIN
DELETE FROM thegame.Invites WHERE (NOW()-InviteTIme)>INTERVAL'00:00:15';
Inviter:=(SELECT PlayerFrom FROM thegame.Invites WHERE PlayerTo=Nick LIMIT 1);
RETURN Inviter;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER;